# GitHub Repositories App

Este é um projeto de demonstração de uma implementação na tentativa de se utilizar boas praticas de desenvolvimento e arquitetura de softwares.

Para demonstração foram criadas:

* Tela de seleção de Linguagem
* Tela de listagem de repositórios de uma linguagem selecionada
* Tela de PullRequests de um repositório selecionado
* Tela de detalhes de um usuário selecionado
* Tela de exibição de usuários favoritados

## Do Projeto

* Linguagem: Kotlin
* Arquitetura: MVVM
* Persistência: Realm
* Injector: Dagger 2
* Requisições: Retrofit 2
* Gestão de Imagens: Glide
* Data Bind: androidx suport DataBind
* Testes de UI: espresso library

## Dependencias
```bash
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
    implementation 'androidx.appcompat:appcompat:1.1.0'
    implementation 'androidx.core:core-ktx:1.1.0'
    implementation 'androidx.constraintlayout:constraintlayout:1.1.3'
    testImplementation 'junit:junit:4.12'
    androidTestImplementation 'androidx.test:runner:1.2.0'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.2.0'

    //support
    implementation 'androidx.recyclerview:recyclerview:1.0.0'
    implementation 'androidx.cardview:cardview:1.0.0'


    //Image
    implementation 'com.github.bumptech.glide:glide:4.10.0'
    annotationProcessor 'com.github.bumptech.glide:compiler:4.10.0'

    //fonts
    implementation 'io.github.inflationx:calligraphy3:3.1.1'
    implementation 'io.github.inflationx:viewpump:2.0.3'

    //Http Services
    implementation 'com.squareup.retrofit2:retrofit:2.4.0'
    implementation 'com.squareup.retrofit2:converter-jackson:2.4.0'
    implementation 'io.reactivex.rxjava2:rxjava:2.2.2'
    implementation 'io.reactivex.rxjava2:rxandroid:2.1.0'
    implementation "com.squareup.retrofit2:adapter-rxjava2:2.3.0"

    //Persistences
    implementation "com.github.vicpinm:krealmextensions:2.2.0"

    //Injectores
    implementation 'com.google.dagger:dagger:2.13'
    kapt 'com.google.dagger:dagger-compiler:2.13'
    kaptAndroidTest 'com.google.dagger:dagger-compiler:2.13'
    compileOnly "org.glassfish:javax.annotation:3.1.1"

    //dataBinding
    kapt 'androidx.databinding:databinding-compiler:3.5.1'

    //tests
    androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.1'
    androidTestImplementation 'com.android.support.test:runner:1.0.1'
    androidTestImplementation 'com.android.support.test.espresso:espresso-intents:3.0.1'


}
```

## Dos testes

Foi criada uma camada simples de testes UI.

## App Demonstração
Segue link para donwload do app Debug
[GitHub Repositories](shorturl.at/bBDI9)