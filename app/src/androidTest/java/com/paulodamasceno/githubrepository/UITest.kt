
import android.support.test.filters.LargeTest
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

import androidx.test.runner.AndroidJUnit4
import com.paulodamasceno.githubrepository.R
import com.paulodamasceno.githubrepository.view.activity.LanguageSelectorActivity

@RunWith(AndroidJUnit4::class)
@LargeTest
class UITest {


    @Rule
    @JvmField
    val rule = IntentsTestRule(LanguageSelectorActivity::class.java)


    @Test
    fun check_LanguageSelectorActivity() {
        onView(withId(R.id.btTestUI)).check(matches(isDisplayed()))
    }

    @Test
    fun click_Kotlin() {
        onView(withId(R.id.btTestUI)).perform(click())
    }
}
