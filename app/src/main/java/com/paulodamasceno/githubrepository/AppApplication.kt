package com.paulodamasceno.githubrepository

import android.app.Application
import com.paulodamasceno.githubrepository.common.Prefs
import io.realm.Realm
import io.realm.RealmConfiguration
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump



/**
 * Created by paulo_frw on 17/04/18.
 */
class AppApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        dbConfig()
        fontConfig()
        prefs = Prefs(this)
    }


    fun fontConfig(){
        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                            .build()
                    )
                )
                .build()
        )
    }

    fun dbConfig(){
        Realm.init(applicationContext)
        var c = RealmConfiguration.Builder()
        c.name("GitHubApi")
        c.deleteRealmIfMigrationNeeded()
        Realm.setDefaultConfiguration(c.build())
    }

    companion object Preferences{
        lateinit var prefs:Prefs
    }


}