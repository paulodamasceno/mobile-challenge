package com.paulodamasceno.githubrepository.Business

import com.paulodamasceno.githubrepository.DAO.FavoriteDAO
import com.paulodamasceno.githubrepository.model.MFavorite
import com.paulodamasceno.githubrepository.model.MUser
import com.paulodamasceno.githubrepository.viewModel.FavoriteViewModel
import java.lang.Exception
import javax.inject.Inject


interface FavoriteContract {
    fun onFavoriteSave() {}
    fun onFavoriteDelete() {}
    fun onFavoriteLoaded(favorite: MFavorite?) {}
    fun onFavoriteListLoaded(favorites: List<MFavorite>) {}
    fun onError(error: String) {}
}


class FavoriteBusiness @Inject constructor() {


    @Inject
    lateinit var favoriteDAO: FavoriteDAO

    @Inject
    lateinit var viewModel: FavoriteViewModel

    lateinit var contract: FavoriteContract

    fun save(favorite: MFavorite){
        try {
            favoriteDAO.save(favorite)
            contract.onFavoriteSave()
        } catch(error: Exception) {
            contract.onError(error.localizedMessage)
        }
    }

    fun save(user: MUser){
        try {

            if (favoriteDAO.get(user.login) != null){
                delete(favoriteDAO.get(user.login)!!)
                contract.onFavoriteSave()
                return
            }


            var favorite = MFavorite()
            favorite.id = user.id
            favorite.avatar = user.avatarUrl!!
            favorite.login = user.login
            favoriteDAO.save(favorite)
            contract.onFavoriteSave()
        } catch(error: Exception) {
            contract.onError(error.localizedMessage)
        }
    }

    fun delete(favorite: MFavorite){
        try {
            favoriteDAO.delete(favorite)
            contract.onFavoriteDelete()
        } catch(error: Exception) {
            contract.onError(error.localizedMessage)
        }
    }

    fun getAll(){
        try {
            viewModel.loadingVisibility.set(false)
            val list = favoriteDAO.getAll()
            viewModel.loadingVisibility.set(list.isEmpty())
            contract.onFavoriteListLoaded(list)
        } catch(error: Exception) {
            viewModel.loadingVisibility.set(true)
            contract.onError(error.localizedMessage)
        }
    }

    fun getItem(login: String){
        try {
            val item = favoriteDAO.get(login)
            contract.onFavoriteLoaded(item)
        } catch(error: Exception) {
            contract.onError(error.localizedMessage)
        }
    }




}