package com.paulodamasceno.githubrepository.Business

import com.paulodamasceno.githubrepository.model.MFavorite

class FavoriteValidator {



    fun haveFavorites(list: ArrayList<MFavorite>) : Boolean {
        return list.isNotEmpty()
    }

    fun containsUser(list: ArrayList<MFavorite>, user: String) : Boolean {
        return  list.filter { it.login.equals(user) }.count() > 0
    }


}