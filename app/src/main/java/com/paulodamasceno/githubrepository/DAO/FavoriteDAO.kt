package com.paulodamasceno.githubrepository.DAO

import com.paulodamasceno.githubrepository.model.MFavorite
import com.vicpin.krealmextensions.*
import dagger.Module
import javax.inject.Inject

@Module
class FavoriteDAO @Inject constructor() {


    fun save(favorite: MFavorite) {
        favorite.save()
    }


    fun getAll() : List<MFavorite> {
        return MFavorite().queryAll()
    }

    fun get(login: String) : MFavorite? {
            return MFavorite().queryFirst { equalTo("login", login) }
    }

    fun delete(favorite: MFavorite) {
        MFavorite().delete { equalTo("login", favorite.login) }
    }



}