package com.paulodamasceno.githubrepository.common

import java.text.SimpleDateFormat
import java.util.*

val sdf = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())

fun Date.getDayOfWeek(): String {
    val calendar = Calendar.getInstance()
    calendar.time = this
    val dayOfWeek:String = when(calendar.get(Calendar.DAY_OF_WEEK)){
        Calendar.MONDAY -> "segunda-feira"
        Calendar.TUESDAY -> "terça-feira"
        Calendar.WEDNESDAY -> "quarta-feira"
        Calendar.THURSDAY -> "quinta-feira"
        Calendar.FRIDAY -> "sexta-feira"
        Calendar.SATURDAY -> "sábado"
        Calendar.SUNDAY -> "domingo"
        else -> {
            ""
        }
    }
    return dayOfWeek
}

fun Date.formatCalendarAdapter(): String{
    val calendar = Calendar.getInstance()
    calendar.time = this
    val dayOfWeek:String = getDayOfWeek()
    return dayOfWeek+ " - " + sdf.format(this)
}

fun String.parseTimeStampToDate(): Date{
    val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
    try {
        return inputFormat.parse(this)
    } catch (exception:Exception) {
        exception.printStackTrace()
        return Date()
    }
}

fun Date.parseToStringTimeStamp(): String{
    val inputFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
    try {
        return inputFormat.format(this)
    } catch (exception:Exception) {
        exception.printStackTrace()
        return ""
    }
}

fun String.parseTimeStampToDate(pattern: String): Date{
    val inputFormat = SimpleDateFormat(pattern, Locale.getDefault())
    return inputFormat.parse(this)
}

fun String.formatCalendarAdapter(): String {
    try {
        return parseTimeStampToDate().formatCalendarAdapter()
    } catch (exception: Exception) {
        exception.printStackTrace()
        return "";
    }

}

fun String.formatOrderDate(): String {
    try {
        val inputFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault())
        val str = this.replace(" +0000", "")
        val date = inputFormat.parse(str)
        return sdf.format(date)
    } catch (throable:Throwable) {
        throable.printStackTrace()
        return "Previsão pendente"
    }
}

fun Date.formatSimpleDate(): String {
    try{
        val inputFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
        return inputFormat.format(this)
    } catch (exception:Exception) {
        exception.printStackTrace()
        return ""
    }
}

fun Date.formatTime(): String {
    try {
        val timeFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        return timeFormat.format(this)
    } catch (exception:Exception) {
        return ""
    }
}

fun Date.formatDateTime():String {
    val inputFormat = SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.getDefault())
    try {
        return inputFormat.format(this)
    } catch (ex:Exception) {
        return ""
    }
}