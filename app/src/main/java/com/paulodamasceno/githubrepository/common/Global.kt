package com.paulodamasceno.githubrepository.common

import com.paulodamasceno.githubrepository.model.MRepository
import com.paulodamasceno.githubrepository.model.MUser

object Global {
    var currentLanguage: String = "Kotlin"
    var currentUser: MUser? = null
    var currentRepository: MRepository? = null
}