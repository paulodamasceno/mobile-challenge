package com.paulodamasceno.githubrepository.common

import android.content.Context
import android.content.SharedPreferences

class Prefs(context: Context) {

    private val PREFS_FILENAME = "com.paulodamasceno.githubresorses.preferences"
    private val BASE_URL = "baseUrl"
    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0);

    var baseUrl: String?
        get() = prefs.getString(BASE_URL, "")
        set(value) = prefs.edit().putString(BASE_URL, value).apply()
}