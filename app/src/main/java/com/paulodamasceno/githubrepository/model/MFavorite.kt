package com.paulodamasceno.githubrepository.model

import io.realm.RealmObject
import io.realm.annotations.RealmClass
import java.io.Serializable

@RealmClass
open class MFavorite(
    var id: Int = 0,
    var login: String = "",
    var avatar: String = ""
) : RealmObject(), Serializable