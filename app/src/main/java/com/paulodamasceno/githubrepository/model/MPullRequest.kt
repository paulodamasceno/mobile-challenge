package com.paulodamasceno.githubrepository.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable
import java.util.*

/**
 * Created by paulodamasceno on 05/11/19.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
open class MPullRequest(
    @JsonProperty("id") var id: Int = 0,
    @JsonProperty("url") var url: String? = "",
    @JsonProperty("state") var state: String = "",
    @JsonProperty("title") var title: String? = "",
    @JsonProperty("body") var body: String? = "",
    @JsonProperty("created_at") var created: Date? = null,
    @JsonProperty("user") var user: MUser?
) : Serializable