package com.paulodamasceno.githubrepository.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.io.Serializable

/**
 * Created by paulodamasceno on 05/11/19.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
open class MRepository(
    @JsonProperty("id") var id: Int = 0,
    @JsonProperty("name") var name: String? = "",
    @JsonProperty("description") var description: String? = "",
    @JsonProperty("language") var language: String? = "",
    @JsonProperty("forks") var forks: Int = 0,
    @JsonProperty("fullName") var fullName: String? = "",
    @JsonProperty("stargazers_count") var stars: Int? = 0,
    @JsonProperty("owner") var user: MUser? = MUser()
) :  Serializable