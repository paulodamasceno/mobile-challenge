package com.paulodamasceno.githubrepository.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
/**
 * Created by paulodamasceno on 05/11/19.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
open class MRootRepository(
    @JsonProperty("total_count") var totalCount: Int = 0,
    @JsonProperty("items") var itens: List<MRepository>?
)