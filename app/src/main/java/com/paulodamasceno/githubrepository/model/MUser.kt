package com.paulodamasceno.githubrepository.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

/**
 * Created by paulodamasceno on 05/11/19.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
open class MUser(
    @JsonProperty("id") var id: Int = 0,
    @JsonProperty("login") var login: String = "",
    @JsonProperty("name") var name: String? = "",
    @JsonProperty("avatar_url") var avatarUrl: String? = "",
    @JsonProperty("type") var type: String? = "",
    @JsonProperty("followers") var followers: Int? = 0,
    @JsonProperty("following") var following: Int? = 0,
    @JsonProperty("created_at") var created: Date? = null
)