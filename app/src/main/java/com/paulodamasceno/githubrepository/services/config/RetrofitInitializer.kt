package com.paulodamasceno.githubrepository
/**
 * Created by paulodamasceno on 05/11/19.
 */

import com.paulodamasceno.githubrepository.common.Constants
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit


object RetrofitInitializer {

    fun retrofit() :Retrofit  {
        if (AppApplication.prefs.baseUrl?.isEmpty()!!) {
            return retrofit(Constants.Network.BASE_URL)
        }
        return retrofit(AppApplication.prefs.baseUrl!!)
    }

    fun retrofit(baseUrl:String) :Retrofit  {

        val httpClient = OkHttpClient.Builder()
        val interceptor = Interceptor {chain ->
            val request = chain.request()?.newBuilder()
                    ?.addHeader("content-type", "application/json")
                    ?.build()
            System.out.println(request)
            chain.proceed(request)
        }

        httpClient.addInterceptor(interceptor)
        httpClient.connectTimeout(15, TimeUnit.SECONDS)
        httpClient.readTimeout(15, TimeUnit.SECONDS)

        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(httpClient.build())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create())
                .build()


        return retrofit
    }

}