package com.paulodamasceno.githubrepository.service.`interface`

import com.paulodamasceno.githubrepository.model.*
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by paulodamasceno on 05/11/19.
 */
interface GitHubService {


    @GET("users/{user}")
    fun user(
        @Path("user") user: String
    ) : Observable<MUser>

    @GET("users/{user}/followers")
    fun followers(
        @Path("user") user: String
    ) : Observable<List<MUser>>

    @GET("search/repositories?sort=stars")
    fun repository(
        @Query("q") language: String,
        @Query("page") page: Int
    ) : Observable<MRootRepository>

    @GET("repos/{owner}/{repository}/pulls")
    fun pullRequest(
        @Path("owner") owner: String,
        @Path("repository") repository: String
    ) : Observable<List<MPullRequest>>



}