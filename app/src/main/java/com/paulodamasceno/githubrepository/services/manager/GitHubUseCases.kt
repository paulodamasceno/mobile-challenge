package com.paulodamasceno.githubrepository.service.manager

import com.paulodamasceno.githubrepository.RetrofitInitializer
import com.paulodamasceno.githubrepository.model.MPullRequest
import com.paulodamasceno.githubrepository.model.MRepository
import com.paulodamasceno.githubrepository.model.MUser
import com.paulodamasceno.githubrepository.service.`interface`.GitHubService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

/**
 * Created by paulodamasceno on 05/11/19.
 */



class GitHubUseCases @Inject constructor() {

    private var useCases:GitHubService = RetrofitInitializer.retrofit().create(GitHubService::class.java)

    fun getRepository(language: String, page: Int) : Observable<List<MRepository>> {
        return useCases.repository(language, page).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).map { it.itens }
    }

    fun getPullRequest(owner: String, repository: String) : Observable<List<MPullRequest>> {
        return useCases.pullRequest(owner, repository).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun getUser(user: String) : Observable<MUser> {
        return useCases.user(user).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun getFollowers(user: String) : Observable<List<MUser>> {
        return useCases.followers(user).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

}