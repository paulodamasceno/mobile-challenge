package com.paulodamasceno.githubrepository.view.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.paulodamasceno.githubrepository.Business.FavoriteBusiness
import com.paulodamasceno.githubrepository.Business.FavoriteContract
import com.paulodamasceno.githubrepository.R
import com.paulodamasceno.githubrepository.databinding.ActivityFavoritesBinding
import com.paulodamasceno.githubrepository.databinding.ActivityUserBinding
import com.paulodamasceno.githubrepository.model.MFavorite
import com.paulodamasceno.githubrepository.view.adapter.ListFavoritesAdapter
import com.paulodamasceno.githubrepository.view.adapter.ListFavoritesListener
import com.paulodamasceno.githubrepository.view.core.BaseActivity
import com.paulodamasceno.githubrepository.view.core.DaggerComponenteInjector
import com.paulodamasceno.githubrepository.viewModel.FavoriteViewModel
import kotlinx.android.synthetic.main.activity_favorites.*
import kotlinx.android.synthetic.main.activity_user.recyclerView
import java.util.ArrayList
import javax.inject.Inject

class FavoritesActivity : BaseActivity(), FavoriteContract, ListFavoritesListener {


    lateinit var mAdapter: ListFavoritesAdapter
    var favorites: List<MFavorite> = ArrayList()
    lateinit var binding: ActivityFavoritesBinding

    @Inject
    lateinit var favoriteBusiness: FavoriteBusiness

    @Inject
    lateinit var viewModel: FavoriteViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorites)
        init()
        initViews()
    }

    fun init(){
        DaggerComponenteInjector.create().inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_favorites)
        binding.viewModel = favoriteBusiness.viewModel
        favoriteBusiness.contract = this
        setSupportActionBar(favoriteToolBar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    fun initViews(){
        mAdapter = ListFavoritesAdapter(favorites, this)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }

        favoriteBusiness.getAll()
    }

    override fun onFavoriteListLoaded(favorites: List<MFavorite>) {
        mAdapter.favorites = favorites
        mAdapter.notifyDataSetChanged()
    }

    override fun onFavoriteclicked(favorite: MFavorite) {
        favoriteBusiness.delete(favorite)
        favoriteBusiness.getAll()
    }


}
