package com.paulodamasceno.githubrepository.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.paulodamasceno.githubrepository.Business.FavoriteBusiness
import com.paulodamasceno.githubrepository.Business.FavoriteContract
import com.paulodamasceno.githubrepository.R
import com.paulodamasceno.githubrepository.common.Global
import com.paulodamasceno.githubrepository.model.MFavorite
import com.paulodamasceno.githubrepository.view.core.BaseActivity
import com.paulodamasceno.githubrepository.view.core.DaggerComponenteInjector
import kotlinx.android.synthetic.main.activity_language_selector.*
import javax.inject.Inject

class LanguageSelectorActivity : BaseActivity(), FavoriteContract {


    @Inject
    lateinit var favoriteBusiness: FavoriteBusiness


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_language_selector)
        DaggerComponenteInjector.create().inject(this)
        favoriteBusiness.contract = this
    }

    override fun onResume() {
        super.onResume()
        favoriteBusiness.getAll()
    }

    fun selectLanguage(view: View){
        Global.currentLanguage = view.tag as String
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    fun showFavoriteUser(view: View){
        val intent = Intent(this, FavoritesActivity::class.java)
        startActivity(intent)
    }

    override fun onFavoriteListLoaded(favorites: List<MFavorite>) {
        if (favorites.isEmpty()) {
            favoriteUserBt.visibility = View.GONE
        } else {
            favoriteUserBt.visibility = View.VISIBLE
        }

    }
}
