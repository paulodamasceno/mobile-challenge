package com.paulodamasceno.githubrepository.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.paulodamasceno.githubrepository.R
import com.paulodamasceno.githubrepository.databinding.ActivityMainBinding
import com.paulodamasceno.githubrepository.model.MRepository
import com.paulodamasceno.githubrepository.view.adapter.ListRepositoryAdapter
import com.paulodamasceno.githubrepository.view.core.BaseActivity
import com.paulodamasceno.githubrepository.view.core.DaggerComponenteInjector
import com.paulodamasceno.githubrepository.viewModel.RepositoryViewContract
import com.paulodamasceno.githubrepository.viewModel.RepositoryViewModel
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject
import androidx.annotation.NonNull
import com.paulodamasceno.githubrepository.common.Global
import com.paulodamasceno.githubrepository.model.MUser
import com.paulodamasceno.githubrepository.view.adapter.RepositoryAdapterListener
import kotlinx.android.synthetic.main.activity_main.recyclerView
import kotlinx.android.synthetic.main.activity_pull_request_list.*


class MainActivity : BaseActivity(), RepositoryViewContract, RepositoryAdapterListener {

    @Inject lateinit var repositoryViewModel : RepositoryViewModel
    lateinit var mAdapter: ListRepositoryAdapter
    lateinit var binding: ActivityMainBinding

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        initViews()
    }

    fun init(){
        DaggerComponenteInjector.create().inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        setSupportActionBar(mainToolBar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title = "Language: ${Global.currentLanguage}"
    }

    fun initViews(){
        binding.viewModel = repositoryViewModel
        mAdapter = ListRepositoryAdapter(repositoryViewModel.repository, this)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }

        infinitScrollView()
        repositoryViewModel.contract = this
        repositoryViewModel.loadRepository()
    }

    fun infinitScrollView(){
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(@NonNull recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
                if (!repositoryViewModel.loadingVisibility.get()) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == repositoryViewModel.repository.size - 1) {
                        repositoryViewModel.loadRepository()
                    }
                }
            }
        })
    }

    override fun onRepositoryLoaded() {
        mAdapter.notifyDataSetChanged()
    }

    override fun onClickRepository(repository: MRepository) {
        Global.currentRepository = repository
        startActivity(Intent(this, PullRequestListActivity::class.java))
    }

    override fun onClickUser(user: MUser) {
        Global.currentUser = user
        startActivity(Intent(this, UserActivity::class.java))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {

            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}


