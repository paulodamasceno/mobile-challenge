package com.paulodamasceno.githubrepository.view.activity

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.MenuItem
import androidx.annotation.NonNull
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.paulodamasceno.githubrepository.R
import com.paulodamasceno.githubrepository.common.Global
import com.paulodamasceno.githubrepository.databinding.ActivityPullRequestListBinding
import com.paulodamasceno.githubrepository.model.MUser
import com.paulodamasceno.githubrepository.view.adapter.ListPullRequestAdapter
import com.paulodamasceno.githubrepository.view.adapter.ListPullRequestListener
import com.paulodamasceno.githubrepository.view.adapter.ListRepositoryAdapter
import com.paulodamasceno.githubrepository.view.core.BaseActivity
import com.paulodamasceno.githubrepository.view.core.DaggerComponenteInjector
import com.paulodamasceno.githubrepository.viewModel.PullRequestViewContract
import com.paulodamasceno.githubrepository.viewModel.PullRequestViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.recyclerView
import kotlinx.android.synthetic.main.activity_pull_request_list.*
import javax.inject.Inject

class PullRequestListActivity : BaseActivity(), PullRequestViewContract, ListPullRequestListener {

    @Inject
    lateinit var pullRequestViewModel: PullRequestViewModel
    lateinit var mAdapter: ListPullRequestAdapter
    lateinit var binding: ActivityPullRequestListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pull_request_list)
        init()
        initViews()
    }

    fun init(){
        DaggerComponenteInjector.create().inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pull_request_list)
        setSupportActionBar(toolBar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        binding.global = Global
    }

    fun initViews(){
        binding.viewModel = pullRequestViewModel
        mAdapter = ListPullRequestAdapter(pullRequestViewModel.pullReuqestList, this)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
        pullRequestViewModel.contract = this
        pullRequestViewModel.loadPullRequest()
    }


    override fun onPullRequestyLoaded() {
        mAdapter.notifyDataSetChanged()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {

            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onClickUser(user: MUser) {
        Global.currentUser = user
        startActivity(Intent(this, UserActivity::class.java))
    }



}
