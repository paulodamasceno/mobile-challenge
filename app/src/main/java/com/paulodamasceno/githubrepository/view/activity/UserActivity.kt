package com.paulodamasceno.githubrepository.view.activity

import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.paulodamasceno.githubrepository.Business.FavoriteBusiness
import com.paulodamasceno.githubrepository.Business.FavoriteContract
import com.paulodamasceno.githubrepository.R
import com.paulodamasceno.githubrepository.common.Global
import com.paulodamasceno.githubrepository.databinding.ActivityUserBinding
import com.paulodamasceno.githubrepository.model.MFavorite
import com.paulodamasceno.githubrepository.model.MUser
import com.paulodamasceno.githubrepository.view.adapter.ListFollowersAdapter
import com.paulodamasceno.githubrepository.view.core.BaseActivity
import com.paulodamasceno.githubrepository.view.core.DaggerComponenteInjector
import com.paulodamasceno.githubrepository.viewModel.UserViewContract
import com.paulodamasceno.githubrepository.viewModel.UserViewModel
import kotlinx.android.synthetic.main.activity_main.recyclerView
import kotlinx.android.synthetic.main.activity_user.*
import java.lang.Exception
import javax.inject.Inject

class UserActivity : BaseActivity(), UserViewContract, FavoriteContract {

    @Inject
    lateinit var userViewModel : UserViewModel
    lateinit var mAdapter: ListFollowersAdapter
    lateinit var binding: ActivityUserBinding

    @Inject
    lateinit var favoriteBusiness: FavoriteBusiness

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)
        init()
        initViews()
    }

    fun init(){
        DaggerComponenteInjector.create().inject(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user)
        setSupportActionBar(userToolBar)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        favoriteBusiness.contract = this
    }

    fun initViews(){
        binding.viewModel = userViewModel
        mAdapter = ListFollowersAdapter(userViewModel.followers)
        recyclerView.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }

        Glide.with(this)
            .load(Global.currentUser!!.avatarUrl)
            .circleCrop()
            .into(imgUserDetail)

        favoriteBt.setOnClickListener {
            try {
                val favorite = it.tag as MUser
                favoriteBusiness.save(favorite)
            }catch(e: Exception){
                e.printStackTrace()
            }
        }

        favoriteBusiness.getItem(Global.currentUser!!.login)
        userViewModel.loadUser()
    }

    override fun onFollowersLoad() {
        mAdapter.notifyDataSetChanged()
    }

    override fun onFavoriteSave() {
        favoriteBusiness.getItem(Global.currentUser!!.login)
    }

    override fun onFavoriteLoaded(favorite: MFavorite?) {
        userViewModel.isFavorite(favorite != null)
    }

    override fun onError(error: String) {
        Log.d("##FAVORITE", error)
    }

}
