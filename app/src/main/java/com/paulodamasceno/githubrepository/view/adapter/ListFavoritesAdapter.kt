package com.paulodamasceno.githubrepository.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.paulodamasceno.githubrepository.R
import com.paulodamasceno.githubrepository.databinding.ListFavoriteItemBinding
import com.paulodamasceno.githubrepository.databinding.ListFollowersItemBinding
import com.paulodamasceno.githubrepository.databinding.ListPullRequestItemBinding
import com.paulodamasceno.githubrepository.model.MFavorite
import com.paulodamasceno.githubrepository.model.MUser
import kotlinx.android.synthetic.main.list_favorite_item.view.*
import kotlinx.android.synthetic.main.list_followers_item.view.*

interface ListFavoritesListener {
    fun onFavoriteclicked(favorite: MFavorite)
}

class ListFavoritesAdapter(
    var favorites: List<MFavorite>, val listener: ListFavoritesListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ListFavoritesViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.list_favorite_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = (holder as ListFavoritesViewHolder).binding
        val favorite = favorites[position]

        binding?.favorite = favorite

        Glide.with(holder.itemView)
            .load(favorite!!.avatar)
            .circleCrop()
            .into(holder.itemView.imgFavorite)

        holder.itemView.favoriteBtItem.setOnClickListener {
            listener.onFavoriteclicked(favorite)
        }


        // binding?.executePendingBindings()
    }

    override fun getItemCount() = favorites.size

}

class ListFavoritesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val binding: ListFavoriteItemBinding? = DataBindingUtil.bind(view)

}
