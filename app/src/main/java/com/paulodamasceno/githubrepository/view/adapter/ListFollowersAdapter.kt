package com.paulodamasceno.githubrepository.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.paulodamasceno.githubrepository.R
import com.paulodamasceno.githubrepository.databinding.ListFollowersItemBinding
import com.paulodamasceno.githubrepository.databinding.ListPullRequestItemBinding
import com.paulodamasceno.githubrepository.model.MUser
import kotlinx.android.synthetic.main.list_followers_item.view.*


class ListFollowersAdapter(
    val followers: List<MUser>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ListFollowersViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.list_followers_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = (holder as ListFollowersViewHolder).binding
        val follower = followers[position]

        binding?.user = follower

        Glide.with(holder.itemView)
            .load(follower!!.avatarUrl)
            .circleCrop()
            .into(holder.itemView.imgFollower)


        // binding?.executePendingBindings()
    }

    override fun getItemCount() = followers.size

}

class ListFollowersViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val binding: ListFollowersItemBinding? = DataBindingUtil.bind(view)

}
