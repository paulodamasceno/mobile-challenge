package com.paulodamasceno.githubrepository.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.paulodamasceno.githubrepository.R
import com.paulodamasceno.githubrepository.common.formatDateTime
import com.paulodamasceno.githubrepository.common.formatSimpleDate
import com.paulodamasceno.githubrepository.databinding.ListPullRequestItemBinding
import com.paulodamasceno.githubrepository.model.MPullRequest
import com.paulodamasceno.githubrepository.model.MUser
import kotlinx.android.synthetic.main.list_pull_request_item.view.*


interface ListPullRequestListener {
    fun onClickUser(user: MUser)
}

class ListPullRequestAdapter(
    val pullRequests: List<MPullRequest>, val listener: ListPullRequestListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ListPullRequestViewHolder(
            LayoutInflater
                .from(parent.context)
                .inflate(R.layout.list_pull_request_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = (holder as ListPullRequestViewHolder).binding
        val pullRequest = pullRequests[position]

        binding?.pullRequest = pullRequest
        binding?.created = pullRequest.created!!.formatDateTime()

        Glide.with(holder.itemView)
            .load(pullRequest.user!!.avatarUrl)
            .circleCrop()
            .into(holder.itemView.imgOwner)

        holder.itemView.userContainerPR.setOnClickListener {
            listener.onClickUser(pullRequest.user!!)
        }

        // binding?.executePendingBindings()
    }

    override fun getItemCount() = pullRequests.size

}

class ListPullRequestViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val binding: ListPullRequestItemBinding? = DataBindingUtil.bind(view)

}
