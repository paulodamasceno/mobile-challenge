package com.paulodamasceno.githubrepository.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.paulodamasceno.githubrepository.R
import com.paulodamasceno.githubrepository.databinding.ListRepositoryItemBinding
import com.paulodamasceno.githubrepository.model.MRepository
import com.paulodamasceno.githubrepository.model.MUser
import kotlinx.android.synthetic.main.list_repository_item.view.*


interface RepositoryAdapterListener {
    fun onClickRepository(repository: MRepository)
    fun onClickUser(user: MUser)
}

class ListRepositoryAdapter(
    val repositories: List<MRepository>, val listener: RepositoryAdapterListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ListRepositoryViewHolder(
            LayoutInflater
            .from(parent.context)
            .inflate(R.layout.list_repository_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val binding = (holder as ListRepositoryViewHolder).binding
        val repository = repositories[position]

        binding?.repository = repository

        Glide.with(holder.itemView)
            .load(repository.user!!.avatarUrl)
            .circleCrop()
            .into(holder.imgUser)

        holder.itemView.setOnClickListener {
            listener.onClickRepository(repository)
        }

        holder.itemView.userContainer.setOnClickListener {
            listener.onClickUser(repository.user!!)
        }

       // binding?.executePendingBindings()
    }

    override fun getItemCount() = repositories.size

}

class ListRepositoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val imgUser = view.imgUser
    val binding: ListRepositoryItemBinding? = DataBindingUtil.bind(view)

}
