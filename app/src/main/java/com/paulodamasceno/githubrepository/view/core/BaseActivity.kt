package com.paulodamasceno.githubrepository.view.core

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.paulodamasceno.githubrepository.view.activity.*
import dagger.Component
import io.github.inflationx.viewpump.ViewPumpContextWrapper



@Component
interface ComponenteInjector {
    fun inject(app: MainActivity)
    fun inject(app: PullRequestListActivity)
    fun inject(app: UserActivity)
    fun inject(app: FavoritesActivity)
    fun inject(app: LanguageSelectorActivity)
}

open class BaseActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId) {

            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

}