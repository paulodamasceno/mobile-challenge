package com.paulodamasceno.githubrepository.viewModel

import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import javax.inject.Inject



class FavoriteViewModel @Inject constructor()  : ViewModel() {
    val loadingVisibility = ObservableBoolean(false)
    val message = ObservableField<String>("nothing to show!")

    fun changeLoading(state: Boolean) {
        loadingVisibility.set(state)
    }
}