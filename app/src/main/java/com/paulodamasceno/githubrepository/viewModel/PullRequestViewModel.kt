package com.paulodamasceno.githubrepository.viewModel

import android.annotation.SuppressLint
import androidx.databinding.Observable
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.paulodamasceno.githubrepository.common.Global
import com.paulodamasceno.githubrepository.model.MPullRequest
import com.paulodamasceno.githubrepository.service.manager.GitHubUseCases
import javax.inject.Inject


interface PullRequestViewContract {
    fun onPullRequestyLoaded()
}

class PullRequestViewModel @Inject constructor() : ViewModel() {

    @Inject
    lateinit var useCases: GitHubUseCases
    lateinit var contract: PullRequestViewContract

    val pullReuqestList = ObservableArrayList<MPullRequest>()
    val loadingVisibility = ObservableBoolean(false)
    val pullOpned = ObservableField<String>()
    val pullClosed = ObservableField<String>()
    val message = ObservableField<String>()
    val wrongShow = ObservableBoolean(false)

    @SuppressLint("CheckResult")
    fun loadPullRequest() {
        loadingVisibility.set(true)
        useCases.getPullRequest(
            Global.currentRepository!!.user!!.login,
            Global.currentRepository!!.name!!
        ).subscribe({
            loadingVisibility.set(false)
            pullReuqestList.addAll(it)
            val opned = it.map { it.state.contains("open") }.toList().count()
            val closed = it.size - opned
            pullOpned.set("$opned\nopned")
            pullClosed.set("$closed\nclosed")
            if (it.isEmpty()) {
                wrongShow.set(true)
                message.set("nothing to show!")
            }
            contract.onPullRequestyLoaded()
        }, {
            loadingVisibility.set(false)
            wrongShow.set(true)
            message.set("what's happening?")
            it.printStackTrace()
        })
    }


}