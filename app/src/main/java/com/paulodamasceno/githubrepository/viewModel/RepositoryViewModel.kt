package com.paulodamasceno.githubrepository.viewModel

import android.annotation.SuppressLint
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.paulodamasceno.githubrepository.common.Global
import com.paulodamasceno.githubrepository.model.MRepository
import com.paulodamasceno.githubrepository.service.manager.GitHubUseCases
import javax.inject.Inject


interface RepositoryViewContract {
    fun onRepositoryLoaded()
}

class RepositoryViewModel @Inject constructor()  : ViewModel() {

    @Inject lateinit var useCases: GitHubUseCases
    lateinit var contract: RepositoryViewContract

    val repository = ObservableArrayList<MRepository>()
    val loadingVisibility = ObservableBoolean(false)
    val message = ObservableField<String>()
    val wrongShow = ObservableBoolean(false)
    var page = 0

    @SuppressLint("CheckResult")
    fun loadRepository(){
        page++
        loadingVisibility.set(true)
        var lang = "language:${Global.currentLanguage}"
        useCases.getRepository(lang,page).subscribe({
            loadingVisibility.set(false)
            repository.addAll(it)
            contract.onRepositoryLoaded()
            if (page == 0 && it.isEmpty()) {
                wrongShow.set(true)
                message.set("nothing found!")
            }
        }, {
            loadingVisibility.set(false)
            wrongShow.set(true)
            message.set("nothing found!")
            it.printStackTrace()
        })
    }


}