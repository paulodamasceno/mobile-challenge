package com.paulodamasceno.githubrepository.viewModel

import android.annotation.SuppressLint
import androidx.databinding.Observable
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.paulodamasceno.githubrepository.common.Global
import com.paulodamasceno.githubrepository.common.formatDateTime
import com.paulodamasceno.githubrepository.model.MPullRequest
import com.paulodamasceno.githubrepository.model.MUser
import com.paulodamasceno.githubrepository.service.manager.GitHubUseCases
import javax.inject.Inject


interface UserViewContract {
    fun onFollowersLoad()
}

class UserViewModel @Inject constructor() : ViewModel() {

    @Inject
    lateinit var useCases: GitHubUseCases
    lateinit var contract: UserViewContract

    val user = ObservableField<MUser>()
    var followers = ObservableArrayList<MUser>()
    val loadingVisibility = ObservableBoolean(false)
    val loadingFoloowersVisibility = ObservableBoolean(false)
    val message = ObservableField<String>()
    val wrongShow = ObservableBoolean(false)
    val created = ObservableField<String>()
    val favorited = ObservableBoolean(false)

    val countFollowers = ObservableField<String>()
    val countFollowing = ObservableField<String>()

    @SuppressLint("CheckResult")
    fun loadUser() {
        loadingVisibility.set(true)
        useCases.getUser(
            Global.currentUser!!.login
        ).subscribe({
            loadingVisibility.set(false)
            if (it == null) {
                wrongShow.set(true)
                message.set("what's happening?")
            } else {
                user.set(it)
                countFollowers.set("${it!!.followers} Followers")
                countFollowing.set("${it!!.following} Following")
                created.set("since: ${it.created!!.formatDateTime()}")
                if (it.followers!! > 0) {
                    loadFollowers()
                } else {
                    wrongShow.set(true)
                    message.set("no followers")
                }
            }
        }, {
            loadingVisibility.set(false)
            wrongShow.set(true)
            message.set("what's happening?")
            it.printStackTrace()
        })
    }

    @SuppressLint("CheckResult")
    fun loadFollowers() {
        loadingFoloowersVisibility.set(true)
        useCases.getFollowers(
            Global.currentUser!!.login
        ).subscribe({
            loadingFoloowersVisibility.set(false)
            followers.addAll(it)
        }, {
            loadingFoloowersVisibility.set(false)
            wrongShow.set(true)
            message.set("what's happening?")
            it.printStackTrace()
        })
    }

    fun isFavorite(favorite: Boolean) {
        this.favorited.set(favorite)
    }


}