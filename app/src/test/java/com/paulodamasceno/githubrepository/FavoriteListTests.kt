package com.paulodamasceno.githubrepository

import com.paulodamasceno.githubrepository.Business.FavoriteValidator
import com.paulodamasceno.githubrepository.model.MFavorite
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * Created by paulodamasceno on 07/11/19.
 */


class FavoriteListTests {


    lateinit var favoriteItens: ArrayList<MFavorite>
    lateinit var favoriteValidator: FavoriteValidator


    @Before
    fun populateList(){
        favoriteValidator = FavoriteValidator()
        favoriteItens = ArrayList()
        favoriteItens.add(MFavorite(1,"Paulo", "paulo"))
        favoriteItens.add(MFavorite(2,"Damasceno", "pcds"))
    }


    @Test
    fun haveRepositories () {
        assert(favoriteValidator.haveFavorites(favoriteItens))
    }


    @Test
    fun containsUser() {
        assert(favoriteValidator.containsUser(favoriteItens, "Damasceno"))
        Assert.assertEquals(false, favoriteValidator.containsUser(favoriteItens, "Paulinho"))
    }



}